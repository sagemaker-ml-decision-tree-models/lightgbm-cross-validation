"""
TRAINING FUNCTIONS: this file in run in 'script mode' when `.fit` is called
from the notebook. `parse_args` and `train_fn` are called in the
`if __name__ =='__main__'` block.
"""
import argparse
import lightgbm
import os
import pickle as pkl
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import cross_val_score


def model_fn(model_dir):
    """Deserialize and return fitted model.
    Note that this should have the same name as the serialized model in the _xgb_train method
    """
    model_file = 'lgbm-model'
    lgbm_model = pkl.load(open(os.path.join(model_dir, model_file), 'rb'))
    return lgbm_model


def train_fn(train_hp, x_input, y_label, x_test, y_test, cv_splits, model_dir):
    """Run LightGBM train on arguments given.
    @param train_hp: Model Hyperparameters.
    @param x_input: train inputs.
    @param y_label: train labels.
    @param x_test: test inputs.
    @param y_test: test labels.
    @param kfold: number of cross validation folds.
    @param model_dir: model artifacts directiry.
    """

    if "regression" in train_hp["objective"]:
        lgbm_model = lightgbm.LGBMRegressor(**train_hp)
    else:
        lgbm_model = lightgbm.LGBMClassifier(**train_hp)

    scores = cross_val_score(lgbm_model, x_input, y_label.values.ravel(), cv=cv_splits, scoring='recall_weighted')
    result = scores.mean()

    # Model Fit
    lgbm_model.fit(x_input, y_label.values.ravel())

    model_location = model_dir + '/lgbm-model'
    pkl.dump(lgbm_model, open(model_location, 'wb'))
    print(f"Stored trained model at {model_location}")
    print(f"Eval Recall: {result}")

    # Test Metrics
    y_true = y_test
    y_pred = lgbm_model.predict(x_test)

    test_recall = metrics.recall_score(y_true, y_pred)
    print(f"Test MAE:  {test_recall}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Hyperparameters are described here.
    parser.add_argument("--tree_boosting_type", type=str, default="gbdt")
    parser.add_argument("--tree_num_leaves", type=int, default=31)
    parser.add_argument("--tree_max_depth", type=int, default=10)
    parser.add_argument("--learning_rate", type=float, default=0.1)
    parser.add_argument("--tree_n_estimators", type=int, default=100)
    parser.add_argument("--subsample_for_bin", type=int)
    parser.add_argument("--objective", type=str)
    parser.add_argument("--class_weight", type=str, default=None)
    parser.add_argument("--min_split_gain", type=float, default=0.)
    parser.add_argument("--min_child_weight", type=float, default=1e-3)
    parser.add_argument("--min_child_samples", type=int, default=20)
    parser.add_argument("--subsample", type=float, default=1.)
    parser.add_argument("--subsample_freq", type=int, default=0)
    parser.add_argument("--colsample_bytree", type=float, default=1.)
    parser.add_argument("--reg_alpha", type=float, default=0.)
    parser.add_argument("--reg_lambda", type=float, default=0.)

    # Cross Validation fold k
    parser.add_argument("--cv_splits", type=int, default=5)

    # Sagemaker System Variables
    parser.add_argument("--model-dir", type=str, default=os.environ.get("SM_MODEL_DIR"))
    parser.add_argument("--schemas", type=str, default=os.environ.get("SM_CHANNEL_SCHEMAS"))
    parser.add_argument("--data-train", type=str, default=os.environ.get("SM_CHANNEL_DATA_TRAIN"))
    parser.add_argument("--label-train", type=str, default=os.environ.get("SM_CHANNEL_LABEL_TRAIN"))
    parser.add_argument("--data-test", type=str, default=os.environ.get("SM_CHANNEL_DATA_TEST"))
    parser.add_argument("--label-test", type=str, default=os.environ.get("SM_CHANNEL_LABEL_TEST"))

    args, _ = parser.parse_known_args()

    # Load Data
    train_set = pd.read_parquet(args.train)
    test_set = pd.read_parquet(args.test)

    x_input = train_set.iloc[:, 1:].values
    y_label = train_set.iloc[:, 0:1].values

    x_test = test_set.iloc[:, 1:].values
    y_test = test_set.iloc[:, 0:1].values

    # Set Hyperparameter arguments
    train_hp = {
        "boosting_type": args.tree_boosting_type,
        "num_leaves": args.tree_num_leaves,
        "max_depth": args.tree_max_depth,
        "learning_rate": args.learning_rate,
        "n_estimators": args.tree_n_estimators,
        "subsample_for_bin": args.subsample_for_bin,
        "objective": args.objective,
        "class_weight": args.class_weight,
        "min_split_gain": args.min_split_gain,
        "min_child_weight": args.min_child_weight,
        "min_child_samples": args.min_child_samples,
        "subsample": args.subsample,
        "subsample_freq": args.subsample_freq,
        "colsample_bytree": args.colsample_bytree,
        "reg_alpha": args.reg_alpha,
        "reg_lambda": args.reg_lambda,
    }

    cv_splits = args.cv_splits

    model_dir = "./output"

    train_fn(train_hp, x_input, y_label, x_test, y_test, cv_splits, model_dir)
